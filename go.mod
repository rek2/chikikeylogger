module gitlab.com/rek2/chikikeylogger.git

go 1.14

require (
	github.com/MarinX/keylogger v0.0.0-20190214225502-bf368a5eb23f
	github.com/sirupsen/logrus v1.4.2
)
